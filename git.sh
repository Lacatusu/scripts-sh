#!/bin/bash

#Declarando variables
dir=$1
touch -c "$dir"/*
inst=$PWD/$dir/node_modules
set -e

#Borrado de node_modules
deletingNodemodules(){
    if [ -d "$inst" ]; then
     rm -rf $inst
     echo "--------->Fichero node_modules eliminado correctamente"
    fi  
}

gitpush(){
  cd $dir
  git add .
  git commit -m "Subida desde bash"
  git push
}

deletingNodemodules
gitpush

npm i
npm audit
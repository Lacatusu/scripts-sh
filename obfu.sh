#!/bin/bash

#Declarando variables
dir=$1
touch -c "$dir"/*
git=$PWD/$dir/.git
set -e

#Función que instala javascript-obfuscator en caso de no tenerla en el equipo
installingObfuscator () {
	if ! [ -x "$(command -v javascript-obfuscator)" ]; then
     echo '------------------------------------------------------------------------------------------------------------------------------------' >&2
     echo '------------------------------------------------------------------------------------------------------------------------------------' >&2
     echo '--------------------------------------------Instalando javascript-obfuscator--------------------------------------------------------' >&2
     npm i -g javascript-obfuscator
    fi
}

#Función que ofusca el código
obfuscating() {
    if [ "$?" = "0" ]; then
     javascript-obfuscator $dir -o /$PWD/obfuscate-files
     yes | cp -i -r $PWD/obfuscate-files/$dir $PWD
     rm -rf $PWD/obfuscate-files
    else
    echo error
    exit
    fi
}

#Función que borra el directorio .git en caso de que exista
deletingGit(){
    if [ -d "$git" ]; then
     rm -rf $git
     echo "Fichero .git eliminado correctamente"
    fi	
}

#Llamada a las funciones para ejecutar la tarea
installingObfuscator
obfuscating
deletingGit

#Generando un archivo .tar
npm pack $dir